#ifndef SIGMOID_H
#define SIGMOID_H

#include "activation_function.h"

class Sigmoid : public Activation_Function
{
public:
    Sigmoid() : m_lambda(1.0f) { }

    virtual ~Sigmoid() { }

    virtual float GetValue(float x) override;

    // derivative can be expressed by the function itself = S(x)(1 - S(x))
    virtual float GetDerivative(float x) override;

private:
    float m_lambda;
};

#endif // SIGMOID_H
