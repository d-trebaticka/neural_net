#include "pooling_layer.h"
#include "activation_function.h"

#include <cstdio>
#include <cmath>
#include <cstdlib>
#include <iostream>


using namespace std;

Pooling_Layer::Pooling_Layer(int width, int height, int slices, int kernel, Activation_Function* func)
    : m_width(width-kernel+1)
    , m_height(height-kernel+1)
    , m_slices(slices)
    , m_kernel_size(kernel)
    , m_pactivation_func(func)
{
    m_ppooling = (float**) malloc(m_slices*sizeof(float*));
    m_pEkwrtyj = (float**) malloc(m_slices*sizeof(float*));
    for(int i = 0; i< m_slices; i++)
    {
        m_ppooling[i] = (float*) malloc((m_width/2)*(m_height/2)*sizeof(float));
        m_pEkwrtyj[i] = (float*) malloc((m_width/2)*(m_height/2)*sizeof(float));
    }

}

Pooling_Layer::~Pooling_Layer()
{
    for(int i = 0; i< m_slices; i++)
    {
        free(m_ppooling[i]);
        free(m_pEkwrtyj[i]);
    }
    free(m_pEkwrtyj);
    free(m_ppooling);
}

void Pooling_Layer::Pool(float** hidden)
{

    //takes maximum from 2x2 area in conv layer
    for(int k = 0; k < m_slices; k++)
    {
        for(int i = 0; i < m_width; i+=2)
        {
            for(int j = 0; j < m_height; j+=2)
            {
                    int indexI = trunc(i/2);
                    int indexJ = trunc(j/2);
                    float max = -100000;
                    float square[4];
                    square[0] = hidden[k][i*m_height + j];
                    square[1] = hidden[k][i*m_height + j + 1];
                    square[2] = hidden[k][(i+1)*m_height + j];
                    square[3] = hidden[k][(i+1)*m_height + j + 1];

                    for(int l=0; l<4; l++)
                    {
                        if(square[l]>max)
                            max = square[l];
                    }
                    m_ppooling[k][indexI*(m_height/2)+indexJ] = max;
            }
        }
    }
}

void Pooling_Layer::calculateEk_wrt_yjDense(float *Ek_wrt_yjDense, float *innerPotential, float **weights, int size)
{
    //this takes calculates of E with respect to y based on weights going to dense layer
    for(int k = 0; k < m_slices; k++)
    {
        for(int i = 0; i < m_width/2; i++)
        {
            for(int j = 0; j < m_height/2; j++)
            {
                float tmp = 0.f;
                for(int r = 0; r < size; r++)
                {
                     tmp+= Ek_wrt_yjDense[r] * m_pactivation_func->GetDerivative(innerPotential[r])
                             * weights[r][k*(m_width/2*m_height/2)+i*(m_height/2)+j];
                }
                m_pEkwrtyj[k][i*(m_height/2)+j] = tmp;


            }
        }

    }
}

/*
void Pooling_Layer::calculateEk_wrt_yjConvolutional(float **Ek_wrt_yjConvolutional, float **innerPotential, float **weights, int slices)
{

    for(int k = 0; k < m_slices; k++)
    {
        for(int i = 0; i < m_width; i++)
        {
            for(int j = 0; j < m_height; j++)
            {
                float tmp = 0;
                for(int l=0; l < slices; l++)
                {
                    for(int x = 0; x < m_kernel_size; x++)
                    {
                        for(int y = 0; y < m_kernel_size; y++)
                        {
                            tmp+=Ek_wrt_yjConvolutional[l*m_slices+k][x*m_kernel_size+y]
                                    *m_pactivation_func->GetDerivative(innerPotential[l*m_slices+k][x*m_kernel_size+y])
                                    *weights[l*m_slices+k][x*m_kernel_size+y];
                        }
                    }

                }

                m_pEkwrtyj[k][i*m_height + j] = tmp;
            }
        }
    }

}*/
