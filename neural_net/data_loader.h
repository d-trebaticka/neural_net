#ifndef DATA_LOADER_H
#define DATA_LOADER_H

#include <string>
#include <fstream>
#include <vector>
#include <iostream>

#include <opencv2/core/core.hpp>

class Data_Loader
{
public:
    /**
     * @brief Data_Loader (constructor)
     * @param path is folder where files are stored
     * @param inputFilesList is name of list containing filenames for loading. Should be on 'path'!
     * @param num_files - how many files
     * @param width - size
     * @param height - size
     */
    Data_Loader(std::string path, std::string inputFilesList, int num_files);

    ~Data_Loader();

    /**
     * @brief LoadData loads from text files to m_pdata and m_labels
     */
    void LoadData();

    float* GetData(int index) { return (float*)m_data[index].data; }

    int GetLabel(int index) { return m_labels[index]; }

    void NormalizeData (float *input);

	int GetNumOfFiles() { return m_num_files; }

private:
    std::string m_pathToData;

    std::ifstream m_inputListFile;      // contains names of files for loading

    //float** m_pdata;                    // array of flattened image data

    int m_num_files;

    std::vector<int> m_labels;

	std::vector<cv::Mat> m_data;

    // TODO: data shuffle.

};

#endif // DATA_LOADER_H
