#ifndef DENSE_LAYER_H
#define DENSE_LAYER_H


class Activation_Function;

class Dense_Layer
{
private:
    int m_size;
    int m_slices;
    int m_dimensionsPooling;
    float m_label;

    float* m_poutput;
    float* m_pinnerPotential;
    float** m_pweights;
    float* m_pEk_wrt_yj;
    float** m_pEk_wrt_wij;
    float** m_pErrorAccumulator;

    Activation_Function* m_pactivation_func;

    void ResetErrorAccumulator();

public:
    Dense_Layer(int size, int slices, int dimensionsPooling, Activation_Function* func);
    ~Dense_Layer();

    void DecidefromPooling(float **pooling);
    void DecidefromDense(float *denseOutput);

    void CalculateErrorLast(float *denseOutput);
    void CalculateErrorMiddle(float **pooling, float *Ek_wrt_yjDense, float *innerPotential,
                              float **weights, int sizeDense2);
    void UpdateWeights(float T, float epsilon); // call after batch
    void setLabel(float label) {m_label = label;}


    float* getEk_wrt_yj() { return m_pEk_wrt_yj; }
    float** getEk_wrt_wij() { return m_pEk_wrt_wij; }
    float** getWeights() { return m_pweights; }
    float* getInnerPotential() { return m_pinnerPotential; }
    float* getOutput() { return m_poutput; }
};

#endif // DENSE_LAYER_H
