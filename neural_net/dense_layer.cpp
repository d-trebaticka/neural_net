#include "dense_layer.h"
#include "activation_function.h"
#include "gauss.h"

#include <cstdio>
#include <cmath>
#include <cstdlib>
#include <iostream>

using namespace std;

Dense_Layer::Dense_Layer(int size, int slices, int dimensionsPooling, Activation_Function* func)
    : m_size(size)
    , m_slices(slices)
    , m_dimensionsPooling(dimensionsPooling) // size of previous layer (could be number of dense output neurons)
    , m_pactivation_func(func)
{
     m_poutput = (float*) malloc(m_size*sizeof(float));
     m_pinnerPotential = (float*) malloc(m_size*sizeof(float));
     m_pEk_wrt_yj = (float*) malloc(m_size*sizeof(float));
     m_pweights = (float**) malloc(m_size*sizeof(float*));

     for(int i = 0; i< m_size; i++)
     {
         m_pweights[i] = (float*) malloc((1+m_dimensionsPooling*m_slices)*sizeof(float)); //+1 because of bias

     }

     m_pEk_wrt_wij = (float**) malloc(m_size*sizeof(float*));
     m_pErrorAccumulator = (float**) malloc(m_size*sizeof(float*));
     for(int i = 0; i< m_size; i++)
     {
         m_pEk_wrt_wij[i] = (float*) malloc((1+m_dimensionsPooling*m_slices)*sizeof(float));
         m_pErrorAccumulator[i] = (float*) malloc((1+m_dimensionsPooling*m_slices)*sizeof(float));
     }

     //initialization of weights and accumulator
     for(int k = 0; k< m_size; k++)
     {
         for(int i = 0; i< 1+m_dimensionsPooling*m_slices; i++)
         {
             m_pErrorAccumulator[k][i]=0;
             m_pweights[k][i] = MyGauss(0.f, 0.33f);
         }
     }

}

Dense_Layer::~Dense_Layer()
{
    free(m_pinnerPotential);
    free(m_poutput);
    free(m_pEk_wrt_yj);
    m_slices = 1;
    for(int i = 0; i< m_size; i++)
    {
        free(m_pweights[i]);
        free(m_pEk_wrt_wij[i]);
        free(m_pErrorAccumulator[i]);
    }
    free(m_pErrorAccumulator);
    free(m_pEk_wrt_wij);
    free(m_pweights);
}

void Dense_Layer::DecidefromPooling(float **pooling)
{
    //calculates inner potential and y if the weights came from the Pooling layer
    for(int k = 0; k < m_size; k++)
    {
        float tmp = 0;
        for(int i = 0; i< m_slices; i++)
        {
            for(int j = 0; j< m_dimensionsPooling; j++)
            {
                tmp +=  m_pweights[k][i*m_dimensionsPooling+j] * pooling[i][j];
            }
        }
        tmp+=m_pweights[k][m_dimensionsPooling*m_slices]; //bias
        m_pinnerPotential[k] = tmp;
        m_poutput[k] = m_pactivation_func->GetValue(tmp);
    }

}

void Dense_Layer::DecidefromDense(float *denseOutput)
{
    //calculates inner potential and y if the weights came from another dense layer
    for(int k = 0; k < m_size; k++)
    {
        float tmp = 0;
        for(int i = 0; i< m_slices; i++)
        {
            for(int j = 0; j< m_dimensionsPooling; j++)
            {
                tmp +=  m_pweights[k][i*m_dimensionsPooling+j] * denseOutput[j];
            }
        }
        tmp+=m_pweights[k][m_dimensionsPooling*m_slices]; //bias
        m_pinnerPotential[k] = tmp;
        m_poutput[k] = m_pactivation_func->GetValue(tmp);
    }

}

void Dense_Layer::CalculateErrorLast(float *denseOutput)
{
    float lambda = 1.0;

    // in case this is the last layer...
    //this calculates derivatives of E with respect to y using y and label
    for(int k = 0; k < m_size; k++)
    {
        m_pEk_wrt_yj[k] = m_poutput[k] - m_label;
    }

    //this calculates derivatives of E with respect to w
    for(int k = 0; k < m_size; k++)
    {
        for(int i = 0; i < m_slices; i++)
        {
            for(int j = 0; j < m_dimensionsPooling; j++)
            {
                m_pEk_wrt_wij[k][i*m_dimensionsPooling+j] = m_pEk_wrt_yj[k] * lambda
                        * m_pactivation_func->GetDerivative(m_pinnerPotential[k]) * denseOutput[j];
                m_pErrorAccumulator[k][i*m_dimensionsPooling+j]+=m_pEk_wrt_wij[k][i*m_dimensionsPooling+j];
            }

        }
    }

}

void Dense_Layer::CalculateErrorMiddle(float **pooling, float *Ek_wrt_yjDense, float *innerPotential, float **weights, int sizeDense2)
{
    float lambda = 1.0;
    //this takes calculates of E with respect to y based on weights going to last dense layer
    for(int k = 0; k < m_size; k++)
    {
        float tmp = 0.f;
        for(int r = 0; r < sizeDense2; r++)
        {
             tmp+= Ek_wrt_yjDense[r] * m_pactivation_func->GetDerivative(innerPotential[r])* weights[r][k];
        }

        m_pEk_wrt_yj[k] = tmp;

    }

    //this takes calculates of E with respect to w based on pooling layer outputs
    for(int k = 0; k < m_size; k++)
    {
        for(int i = 0; i < m_slices; i++)
        {
            for(int j = 0; j < m_dimensionsPooling; j++)
            {
                m_pEk_wrt_wij[k][i*m_dimensionsPooling+j] = m_pEk_wrt_yj[k] * lambda
                        * m_pactivation_func->GetDerivative(m_pinnerPotential[k]) * pooling[i][j];
                m_pErrorAccumulator[k][i*m_dimensionsPooling+j]+=m_pEk_wrt_wij[k][i*m_dimensionsPooling+j];
            }

        }
    }

}

void Dense_Layer::UpdateWeights(float T, float epsilon)
{
    //updates weights
    for(int k = 0; k < m_size; k++)
    {
        for(int i = 0; i < m_slices; i++)
        {
            for(int j = 0; j < m_dimensionsPooling; j++)
            {
                float deltaW = -epsilon*(m_pErrorAccumulator[k][i*m_dimensionsPooling+j]/T);
                m_pweights[k][i*m_dimensionsPooling+j]+=deltaW;
            }
        }
    }

    ResetErrorAccumulator();

}

void Dense_Layer::ResetErrorAccumulator()
{
    for(int k = 0; k < m_size; k++)
    {
        for(int i = 0; i < m_slices; i++)
        {
            for(int j = 0; j < m_dimensionsPooling; j++)
            {
                m_pErrorAccumulator[k][i*m_dimensionsPooling+j] = 0.f;
            }
        }
    }

}
