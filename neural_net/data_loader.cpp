#include "data_loader.h"

#include <opencv2/highgui/highgui.hpp>

using namespace std;

Data_Loader::Data_Loader(std::string path, std::string inputFilesList, int num_files)
    : m_pathToData(path)
    , m_num_files(num_files)
{
    m_inputListFile.open((path+inputFilesList).c_str());

    m_labels.resize(num_files);
	m_data.resize(num_files);
}

Data_Loader::~Data_Loader()
{
    if (m_inputListFile.is_open())
    {
        m_inputListFile.close();
    }
}

void Data_Loader::LoadData()
{
    std::string filename;
	cv::Mat img;

	for (int i = 0; i < m_num_files; ++i)
    {
		m_inputListFile >> filename;

		img = cv::imread(m_pathToData + filename, CV_LOAD_IMAGE_GRAYSCALE);
		cv::Mat img_f;
		img.convertTo(img_f, CV_32F);
		m_data[i] = img_f;

        m_inputListFile >> m_labels[i];

    }
}

 void Data_Loader::NormalizeData (float* input)
 {
	 int size = m_data[0].cols*m_data[0].rows;

     float mean = 0.f;
	 for (int i = 0; i < size; i++)
     {
         mean+=input[i];
     }
	 mean /= size;
	 for (int i = 0; i < size; i++)
     {
         input[i]-=mean;
     }

     float low = 5000000.f;
     float high = -5000000.f;
	 for (int i = 0; i < size; i++)
     {
         if(input[i] > high)
             high = input[i];
         if(input[i] < low)
             low = input[i];
     }

	 for (int i = 0; i < size; i++)
     {
         input[i] = (input[i]-low)*(1-(-1))/(high-low)+(-1);
     }
 }
