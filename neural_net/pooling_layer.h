#ifndef POOLING_LAYER_H
#define POOLING_LAYER_H

class Activation_Function;

class Pooling_Layer
{
private:
    int m_width;
    int m_height;
    int m_slices;
    int m_kernel_size;      // size of conv kernel of previous layer

    float** m_ppooling;     // output after forward pass
    float** m_pEkwrtyj;     // gradient of error w.r.t. y_j

    Activation_Function* m_pactivation_func;

public:
    Pooling_Layer(int width, int height, int slices, int kernel, Activation_Function* func);
    ~Pooling_Layer();

    /**
     * @brief Pool calculates pooling 2x2 with stride 2(forward pass)
     * @param hidden - output from convolution
     */
    void Pool(float **hidden);

    /** backward pass */
    void  calculateEk_wrt_yjDense(float *Ek_wrt_yjDense, float*innerPotential, float **weights, int size);
    // void  calculateEk_wrt_yjConvolutional(float **Ek_wrt_yjConvolutional, float **innerPotential, float **weights, int slices);

    /** getters (in case you couldn't tell) */
    float** getPooling() { return m_ppooling; }
    float**  getEk_wrt_yj() { return m_pEkwrtyj; }
};

#endif // POOLING_LAYER_H
