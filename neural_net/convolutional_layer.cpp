#include "convolutional_layer.h"
#include "activation_function.h"
#include "gauss.h"

#include <cstdio>
#include <cmath>
#include <cstdlib>
#include <iostream>

using namespace std;

Convolutional_Layer::Convolutional_Layer(int width, int height, int slices, int kernel, Activation_Function* func)
    : m_width(width-kernel+1)
    , m_height(height-kernel+1)
    , m_num_kernels(slices)
    , m_kernel(kernel)
    , m_pactivation_func(func)
{
    // feature maps: num_slices_i*num_kernels*h_o*w_o (h_o=h_i-kernel+1; w_o likewise)
    m_poutput = (float**) malloc(m_num_kernels*sizeof(float*));
    m_pEkwrtyj = (float**) malloc(m_num_kernels*sizeof(float*));
    m_pinnerPotental = (float**) malloc(m_num_kernels*sizeof(float*));
    for(int i = 0; i < m_num_kernels; i++)
    {
        m_poutput[i] = (float*) malloc(m_width*m_height*sizeof(float));
        m_pinnerPotental[i] = (float*) malloc(m_width*m_height*sizeof(float));
        m_pEkwrtyj[i] = (float*) malloc(m_width*m_height*sizeof(float));
    }

    // features: num_kernels*num_slices_i*kernel*kernel
    m_pweights = (float**) malloc(m_num_kernels*sizeof(float*));
    m_pEkwrtwij = (float**) malloc(m_num_kernels*sizeof(float*));
    m_pErrorAccumulator = (float**) malloc(m_num_kernels*sizeof(float*));
    for(int i = 0; i < m_num_kernels; i++)
    {
        m_pweights[i] = (float*) malloc(kernel*kernel*sizeof(float));
        m_pEkwrtwij[i] = (float*) malloc(kernel*kernel*sizeof(float));
        m_pErrorAccumulator[i] = (float*) malloc(kernel*kernel*sizeof(float));
    }

    //initialization of weights and accumulator
    for(int i = 0; i < m_num_kernels; i++)
    {
        for(int j = 0; j < m_kernel*m_kernel; j++)
        {
            m_pErrorAccumulator[i][j] = 0;
            m_pweights[i][j] = MyGauss(0.f, 0.33f);

        }
    }

}

Convolutional_Layer::~Convolutional_Layer()
{
    for(int i = 0; i < m_num_kernels; i++)
    {
        free(m_pweights[i]);
        free(m_poutput[i]);
        free(m_pinnerPotental[i]);
        free(m_pEkwrtwij[i]);
        free(m_pEkwrtyj[i]);
        free(m_pErrorAccumulator[i]);
    }
    free(m_pErrorAccumulator);
    free(m_pEkwrtyj);
    free(m_pEkwrtwij);
    free(m_pinnerPotental);
    free(m_poutput);
    free(m_pweights);
}


void Convolutional_Layer::Convolve(float* input)
{
    int heightInput = m_height+m_kernel-1;

    //does convolution of the image with each of the kernels
    for(int k = 0; k < m_num_kernels; k++)
    {
        for(int i = 0; i < m_width; i++)
        {
            for(int j = 0; j < m_height; j++)
            {
                float tmp = 0;
                for(int u = 0; u < m_kernel; u++)
                {
                    for(int v = 0; v < m_kernel; v++)
                    {
                        tmp+=input[(i+u)*heightInput + (j+v)]*m_pweights[k][u*m_kernel+v];
                    }
                }
                m_pinnerPotental[k][i*m_height + j] = tmp;
                m_poutput[k][i*m_height + j] = m_pactivation_func->GetValue(tmp);
            }
        }
    }


}

void Convolutional_Layer::CalculateError(float **EkwrtyjPooling)
{

    //this takes derivative of E with respect to y from pooling layer based on the argmax (2x2 pooling)
    for(int k = 0; k < m_num_kernels; k++)
    {
        for(int i = 0; i < m_width; i+=2)
        {
            for(int j = 0; j < m_height; j+=2)
            {

                    int indexI = trunc(i/2);
                    int indexJ = trunc(j/2);
                    float square[4];
                    square[0] = m_poutput[k][i*m_height + j];
                    square[1] = m_poutput[k][i*m_height + j + 1];
                    square[2] = m_poutput[k][(i+1)*m_height + j];
                    square[3] = m_poutput[k][(i+1)*m_height + j + 1];

                    m_pEkwrtyj[k][i*m_height + j] = 0.f;
                    m_pEkwrtyj[k][i*m_height + j + 1] = 0.f;
                    m_pEkwrtyj[k][(i+1)*m_height + j] = 0.f;
                    m_pEkwrtyj[k][(i+1)*m_height + j + 1] = 0.f;

                    float max = -1000000.f;
                    int iter, iter2;
                    for(int h=0; h<4; h++)
                    {
                        if(square[h]>max)
                        {
                            max = square[h];
                            iter = h;
                        }
                    }

                    switch(iter)
                    {
                        case 0:
                        {
                            iter2 =  i*m_height + j;
                            break;
                        }
                        case 1:
                        {
                            iter2 =  i*m_height + j + 1;
                            break;
                        }
                        case 2:
                        {
                            iter2 =  (i+1)*m_height + j;
                            break;
                        }
                        case 3:
                        {
                            iter2 =  (i+1)*m_height + j + 1;
                            break;
                        }
                    }
                    m_pEkwrtyj[k][iter2] = EkwrtyjPooling[k][indexI*(m_height/2)+indexJ];

            }
        }
    }



    float lambda = 1.0;

    //this calculates derivative of E with respect to w
    for(int k = 0; k < m_num_kernels; k++)
    {
        for(int i = 0; i < m_kernel; i++)
        {
            for(int j = 0; j < m_kernel; j++)
            {
                float tmp = 0.f;

                for(int x = 0; x < m_width; x++)
                {
                    for(int y = 0; y < m_height; y++)
                    {

                        tmp+=m_pEkwrtyj[k][x*m_height + y]*lambda
                                * m_pactivation_func->GetDerivative(m_pinnerPotental[k][x*m_height + y])
                                * m_poutput[k][x*m_height + y];
                    }
                }
                m_pEkwrtwij[k][i*m_kernel+j] = tmp;
                m_pErrorAccumulator[k][i*m_kernel+j] += m_pEkwrtwij[k][i*m_kernel+j];
            }
        }
    }

}

void Convolutional_Layer::UpdateWeights(float T, float epsilon)
{

        //updates weights
        for(int k = 0; k< m_num_kernels; k++)
        {
            for(int i = 0; i< m_kernel; i++)
            {
                for(int j = 0; j< m_kernel; j++)
                {
                    float deltaW = -epsilon* m_pErrorAccumulator[k][i*m_kernel+j]/T;
                    m_pweights[k][i*m_kernel+j]+=deltaW;
                }
            }
        }

        ResetErrorAccumulator();

}

void Convolutional_Layer::ResetErrorAccumulator()
{
    for(int k = 0; k < m_num_kernels; k++)
    {
        for(int i = 0; i < m_kernel; i++)
        {
            for(int j = 0; j < m_kernel; j++)
            {
                m_pErrorAccumulator[k][i*m_kernel+j] = 0.f;
            }
        }
    }
}
