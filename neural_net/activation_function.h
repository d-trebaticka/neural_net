#ifndef ACTIVATION_FUNCTION_H
#define ACTIVATION_FUNCTION_H


class Activation_Function
{
public:
    Activation_Function() { }

    virtual ~Activation_Function() { }


    /**
     * @brief calculate function(x)
     * @param x argument for function
     * @return calculated value
     */
    virtual float GetValue(float x) = 0;

    /**
     * @brief GetDerivative
     * @param x
     * @return derivative of function
     */
    virtual float GetDerivative(float x) = 0;
};

#endif // ACTIVATION_FUNCTION_H
