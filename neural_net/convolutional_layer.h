#ifndef CONVOLUTIONAL_LAYER_H
#define CONVOLUTIONAL_LAYER_H


class Activation_Function;

class Convolutional_Layer
{
private:
    int m_width;            // image width (x)
    int m_height;           // image height (y)
    int m_num_kernels;      // number of trained kernels
    int m_kernel;           // kernel size

    float** m_poutput;          // output of neurons after computation
    float** m_pinnerPotental;   // inner potential of neurons
    float** m_pweights;         // shared weights
    float** m_pEkwrtyj;         // gradient of error w.r.t. output value of neuron
    float** m_pEkwrtwij;        // gradient of error w.r.t. weight
    float** m_pErrorAccumulator;// error accumulator

    Activation_Function* m_pactivation_func;    // we don't own this resource.

    void ResetErrorAccumulator();

public:
    Convolutional_Layer(int width, int height, int slices, int kernel, Activation_Function* func);
    ~Convolutional_Layer();

    /**
     * @brief Convolve calculates neuron output in forward pass and inner potential of neuron
     * @param input - data to process
     */
    void Convolve(float *input);

    /**
     * @brief CalculateError
     * @param EkwrtyjPooling
     */
    void CalculateError(float **EkwrtyjPooling);

    /**
     * @brief UpdateWeights
     */
    void UpdateWeights(float T, float epsilon);

    /** getters */
    float** getHidden() { return m_poutput; }
    float** getInnerPotential() { return m_pinnerPotental; }
    float** getErrorAccumulator() { return m_pErrorAccumulator; }
    float** getWeights() { return m_pweights; }
    float** getEk_wrt_yj() { return m_pEkwrtyj; }
    float** getEk_wrt_wij() { return m_pEkwrtwij; }
};

#endif // CONVOLUTIONAL_LAYER_H
