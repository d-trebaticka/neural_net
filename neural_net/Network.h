#pragma once

#include <string>

class Activation_Function;
class Data_Loader;
class Convolutional_Layer;
class Pooling_Layer;
class Dense_Layer;

class Network
{
public:
	Network(int width, int height);

	~Network();

	void CreateNetwork();

	void TrainBatch(Data_Loader* loader);

	float Classify(float* input, int& result);

private:
	int m_img_width = 50;
	int m_img_height = 50;
	int m_kernel_size = 5;
	int m_output_kernels = 16;
	int m_dense_output_size1 = 30;
	int m_dense_output_size2 = 1;

	Activation_Function* m_pact_func;

	Convolutional_Layer* ConvFirst;
	Pooling_Layer* PoolFirst;
	Dense_Layer* Dense1;
	Dense_Layer* Dense2;

	void Forward();

	void Backward();

};

