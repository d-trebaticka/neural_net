#include "sigmoid.h"
#include <math.h>

float Sigmoid::GetValue(float x)
{
    return 1/(1+exp(-m_lambda*x));
}

float Sigmoid::GetDerivative(float x)
{
    return GetValue(x)*(1-GetValue(x));
}
