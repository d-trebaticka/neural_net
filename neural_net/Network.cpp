#include "Network.h"

#include "convolutional_layer.h"
#include "pooling_layer.h"
#include "dense_layer.h"
#include "data_loader.h"
#include "sigmoid.h"

Network::Network(int width, int height)
	: m_img_width(width)
	, m_img_height(height)
	, m_kernel_size(5)
	, m_output_kernels(16)
	, m_dense_output_size1(30)
	, m_dense_output_size2(1)
	, m_pact_func(new Sigmoid())
	, ConvFirst(nullptr)
	, PoolFirst(nullptr)
	, Dense1(nullptr)
	, Dense2(nullptr)
{
	CreateNetwork();
}


Network::~Network()
{
	delete m_pact_func;
	delete ConvFirst;
	delete PoolFirst;
	delete Dense1;
	delete Dense2;
}

void Network::CreateNetwork()
{
	ConvFirst = new Convolutional_Layer(m_img_width, m_img_height, m_output_kernels, m_kernel_size, m_pact_func);

	PoolFirst = new Pooling_Layer(m_img_width, m_img_height, m_output_kernels, m_kernel_size, m_pact_func);

	Dense1 = new Dense_Layer(m_dense_output_size1, m_output_kernels, (m_img_width - m_kernel_size + 1) / 2 * (m_img_height - m_kernel_size + 1) / 2, m_pact_func);

	Dense2 = new Dense_Layer(m_dense_output_size2, 1, m_dense_output_size1, m_pact_func);
}

void Network::TrainBatch(Data_Loader* loader)
{
	for (int i = 0; i<loader->GetNumOfFiles(); i++)
	{
		float* input;
		input = loader->GetData(i);
		int label = loader->GetLabel(i);

		loader->NormalizeData(input);

		//std::cout << "LABEL " << label << std::endl << std::endl;
		//printInput(input, width, "INPUT");

		if (label == 0)
			label = 0;
		else
			label = 1;
		Dense2->setLabel(label);

		// ----------- TRAIN
		// ----------- FORWARD PASS
		ConvFirst->Convolve(input);
		//print(ConvFirst->getWeights(), kernel_size, output_kernels, "CONV1 WEIGHTS");
		//print(ConvFirst->getInnerPotential(), (width-kernel_size+1), output_kernels, "CONV1 INNER POTENTIAL");
		//print(ConvFirst->getHidden(), (width-kernel_size+1), output_kernels, "CONV1 OUTPUT");

		PoolFirst->Pool(ConvFirst->getHidden());
		//print(PoolFirst->getPooling(), ((width-kernel_size+1)/2), output_kernels, "POOL1");

		Dense1->DecidefromPooling(PoolFirst->getPooling());
		//printDense(Dense1->getWeights(), output_kernels, ((width-kernel_size+1)/2)*((width-kernel_size+1)/2), dense_output_size1, "DENSE WEIGHTS");
		//print(Dense1->getInnerPotential(), dense_output_size1, "DENSE INNER POTENTIAL");
		//print(Dense1->getOutput(), dense_output_size1, "DENSE OUTPUT");

		Dense2->DecidefromDense(Dense1->getOutput());
		//if(i ==0 && j==0)
		//printDense(Dense2->getWeights(), dense_output_size1, 1, dense_output_size2, "DENSE WEIGHTS");
		//print(Dense2->getInnerPotential(), dense_output_size2, "DENSE INNER POTENTIAL");
		//print(Dense2->getOutput(), dense_output_size2, "DENSE OUTPUT");

		// ---------- BACKPROP
		Dense2->CalculateErrorLast(Dense1->getOutput());
		//print(Dense2->getEk_wrt_yj(), dense_output_size2,"DENSE GRADIENT WRT Y");
		//printDense(Dense2->getEk_wrt_wij(),  dense_output_size1, 1, dense_output_size2,"DENSE GRADIENT WRT W");
		Dense1->CalculateErrorMiddle(PoolFirst->getPooling(), Dense2->getEk_wrt_yj(), Dense2->getInnerPotential(), Dense2->getWeights(), m_dense_output_size2);
		//print(Dense1->getEk_wrt_yj(), dense_output_size1,"DENSE GRADIENT WRT Y");
		//printDense(Dense1->getEk_wrt_wij(),  output_kernels, ((width-kernel_size+1)/2)*((width-kernel_size+1)/2), dense_output_size1,"DENSE GRADIENT WRT W");


		PoolFirst->calculateEk_wrt_yjDense(Dense1->getEk_wrt_yj(), Dense1->getInnerPotential(), Dense1->getWeights(), m_dense_output_size1);
		//print(PoolFirst->getEk_wrt_yj(), ((width-kernel_size+1)/2),output_kernels,"POOL1 GRADIENT WRT Y");


		ConvFirst->CalculateError(PoolFirst->getEk_wrt_yj());
		//print(ConvFirst->getEk_wrt_yj(), ((width-kernel_size+1)),output_kernels,"CONV1 GRADIENT WRT Y");
		//print(ConvFirst->getEk_wrt_wij(), kernel_size,output_kernels,"CONV1 GRADIENT WRT W");
	}

	//weights get updated after each batch

	Dense2->UpdateWeights(20.f, 0.1f/*learning rate*/);
	//printDense(Dense2->getWeights(), dense_output_size1, 1, dense_output_size2, "DENSE2 WEIGHTS");
	Dense1->UpdateWeights(20.f, 0.1f/*learning rate*/);
	//printDense(Dense1->getWeights(), output_kernels, ((width-kernel_size+1)/2)*((width-kernel_size+1)/2), dense_output_size1, "DENSE1 WEIGHTS");
	ConvFirst->UpdateWeights(20.f, 0.1f);
	//print(ConvFirst->getWeights(), kernel_size, output_kernels, "CONV1 WEIGHTS");
}

float Network::Classify(float* input, int& result)
{
	float threshold = 0.4f; //threshold to decide if 1 or 0 in the final neuron

	ConvFirst->Convolve(input);
	PoolFirst->Pool(ConvFirst->getHidden());
	Dense1->DecidefromPooling(PoolFirst->getPooling());
	Dense2->DecidefromDense(Dense1->getOutput());
	
	result = ((Dense2->getOutput()[0] > threshold) ? 1 : 0);

	return Dense2->getOutput()[0];
}

void Network::Forward()
{

}

void Network::Backward()
{

}


