#include <cstdio>
#include <cmath>
#include <cstdlib>
#include <iostream>
#include <vector>
#include <time.h>
#include <cstring>
#include <string>

#include "network.h"
#include "data_loader.h"

using namespace std;

void print(float** input, int side, int slices, std::string label)
{
    std::cout << label << std::endl;
    for (int k = 0; k < slices; ++k)
    {
        for (int i = 0; i < side; ++i)
        {
            for (int j = 0; j < side; ++j)
            {
                std::cout << input[k][i*side+j] << " ";
            }
            std::cout << std::endl;
        }
        std::cout << std::endl;
    }

    std::cout << std::endl;
}

void printDense(float** input, int output_kernels, int side, int slices, std::string label)
{
    std::cout << label << std::endl;
    for (int k = 0; k < slices; ++k)
    {
        for (int i = 0; i < output_kernels; ++i)
        {
            for (int j = 0; j < side; ++j)
            {
                std::cout << input[k][i*side+j] << " ";
            }
            std::cout << std::endl;
        }
        std::cout << std::endl;
    }

    std::cout << std::endl;
}

void printInput(float* input, int side, std::string label)
{
    std::cout << label << std::endl;

    for (int i = 0; i < side; ++i)
    {
        for (int j = 0; j < side; ++j)
        {
            std::cout << input[i*side+j] << " ";
        }
        std::cout << std::endl;
    }


    std::cout << std::endl;
}

void print(float* input, int size,std::string label)
{
    std::cout << label << std::endl;
    for (int j = 0; j < size; ++j)
    {
        std::cout << input[j] << " ";
    }
    std::cout << std::endl << std::endl;
}

void createBatches(string name, string type, string path,
				   int batchSize, int numBatches, int numFilesPerClass)
{
	time_t t;
	srand((unsigned) time(&t));

	for (int i = 0; i < numBatches; ++i)
	{
		std::ofstream file(path + "batch" + to_string(i) + ".txt");

		for (int k = 0; k < 2; ++k) // labels
		{
			for (int n = 0; n < batchSize / 2; ++n)
			{
				if (name.size() > 0)
				{
					file << name << "_";
				}

				file << k << "_" << rand() % numFilesPerClass << type 
					<< " " << k << endl;
			}
		}

		file.close();
	}
}

int main()
{
	std::string path("C:\\Users\\dominika\\Desktop\\MUNI\\Mgr\\I\\PV021\\neural_net\\neural_net\\test_batch\\");

	// init just width and height of image, rest of the values is hardcoded in Network class
	Network net(28, 28);

	int batches = 300;
	int batchSize = 50;
	int numSamplesPerClass = 2000;	// we need the same number of samples from both classes!
	std::string imgname("mnist");	// in case of our data it's empty string: ""
	createBatches(imgname, ".jpg", path, batchSize, batches, numSamplesPerClass);

	//this is just to check it several times, the batches are the same
	//but the weights are initialized differently every time. if you comment it, only one run will happen
    //for(int z = 0; z < 10; z++) 
	{
		// start from second batch, we keep the first batch for validating
		for(int j = 1; j < batches; j++) //GOES THROUGH ALL 500 BATCHES
		{
		    string list("batch");
		    list += to_string(j);
		    list += ".txt";

			Data_Loader *loader = new Data_Loader(path, list, batchSize);
			loader->LoadData(); // loads ALL data in batch

			net.TrainBatch(loader);

			delete loader;

			cout << "batch " << j << endl;
		}

	}

	// VALIDATE
	int numValData = batchSize;
	Data_Loader* loader2 = new Data_Loader(path, "batch0.txt", numValData);
	loader2->LoadData();

	int YAYcounter = 0, NAYcounter = 0;
	float ZEROavg = 0, ONEavg = 0;

	float* input;

	for (int i = 0; i < loader2->GetNumOfFiles(); ++i)
	{
		input = loader2->GetData(i);
		int label = loader2->GetLabel(i);
		loader2->NormalizeData(input);

		int resultLabel = -1;
		float val = net.Classify(input, resultLabel);

		if (label == 1)
		{
			ONEavg += val;
		}
		else if (label == 0)
		{
			ZEROavg += val;
		}

		if (resultLabel == label)
		{
			YAYcounter++;
		}
		else
		{
			NAYcounter++;
		}
	}

	delete loader2;

	ZEROavg /= numValData / 2;
	ONEavg /= numValData / 2;
	cout << "YAY: " << YAYcounter << "\t\tNAY: " << NAYcounter << endl; //how many were right/wrong
	cout << "ZEROavg: " << ZEROavg << "\t\tONEavg: " << ONEavg << endl; //average final neuron value for 0 and 1

	return 0;
}


